<?php

/**
 * @file
 * Drush commands for Content Purge.
 */

/**
 * Implements hook_drush_command().
 */
function content_purge_drush_command() {
  $items = [];

  $items['content-purge-entity-bundle'] = [
    'callback' => 'drush_content_purge_entity_bundle',
    'description' => 'Purge a content type. Deletes all content, fields, and the content type.',
    'examples' => [
      'drush content-purge-entity-bundle page',
      'drush content-purge-entity-bundle article node',
    ],
    'arguments' => [
      'bundle' => 'The bundle to purge (e.g. article). Required.',
      'entity_type' => 'The entity_type for the bundle (e.g. node). Defaults to node.',
    ],
  ];

  $items['content-purge-file-type'] = [
    'callback' => 'drush_content_purge_file_type',
    'description' => 'Purge a file type. Deletes all files, fields, and the file type.',
    'examples' => [
      'drush content-purge-file-type banner_image',
    ],
    'arguments' => [
      'file_type' => 'The file_type to purge (e.g. banner_image). Required.',
    ],
  ];

  $items['content-purge-feature'] = [
    'callback' => 'drush_content_purge_feature',
    'description' => 'Disable a feature and delete all content, fields, and the content type.',
    'examples' => [
      'drush content-purge-feature myfeature',
    ],
    'arguments' => [
      'name' => 'The name of the feature to purge. Required.',
    ],
  ];

  $items['content-purge-feature-fields'] = [
    'callback' => 'drush_content_purge_feature_fields',
    'description' => 'Purge fields and groups from a content type and re-export the feature.',
    'examples' => [
      'drush content-purge-feature-fields featurename contenttype fields groups',
    ],
    'arguments' => [
      'name' => 'The name of the feature. Required.',
      'bundle' => 'The bundle name. Required.',
      'fields' => 'The field names to purge.',
      'groups' => 'The group names to purge.',
    ],
  ];

  $items['content-purge-missing-files'] = [
    'callback' => 'drush_content_purge_missing_files',
    'description' => 'Purge missing files from the file_managed table.',
    'examples' => [
      'drush content-purge-missing-files',
    ],
  ];

  return $items;
}

/**
 * Drush command callback. Purge content, content type and fields.
 *
 * @param string $bundle
 * @param string $entity_type
 *
 * @throws \Exception
 */
function drush_content_purge_entity_bundle($bundle, $entity_type = 'node') {
  if ($bundle === NULL) {
    drush_set_error('bundle', dt('You must specify a bundle to purge.'));
  }

  drush_log(dt('Delete @bundle @entity_type entities.', ['@bundle' => $bundle, '@entity_type' => $entity_type]), 'ok');

  // Delete the entities.
  $count = content_purge_delete_entities($bundle, $entity_type);
  drush_log(dt('  Deleted @count @bundle @entity_types.', ['@count' => $count, '@bundle' => $bundle, '@entity_type' => $entity_type]), 'ok');

  // Delete the content type.
  switch ($entity_type) {
    case 'node':
      if (node_type_load($bundle)) {
        node_type_delete($bundle);
        variable_del('node_preview_' . $bundle);
        node_types_rebuild();
//        field_purge_batch(1000);
        menu_rebuild();
        drush_log(dt('  Deleted @bundle content type.', ['@bundle' => $bundle]), 'ok');
      }
      break;
  }
}

/**
 * Drush command callback. Purge files, file type and fields.
 *
 * @param string $file_type
 *   The file type.
 *
 * @throws \Exception
 */
function drush_content_purge_file_type($file_type) {
  if ($file_type === NULL) {
    drush_set_error('file_type', dt('You must specify a file_type to purge.'));
  }

  drush_log(dt('Delete @file_type files.', ['@file_type' => $file_type]), 'ok');

  // Delete the files.
  $count = content_purge_delete_file_entities($file_type);
  drush_log(dt('  Deleted @count @file_type file entities.', ['@count' => $count, '@file_type' => $file_type]), 'ok');

  // Delete the file type.
  if (file_type_load($file_type)) {
    file_type_delete($file_type);
    drush_log(dt('  Deleted @file_type file type.', ['@file_type' => $file_type]), 'ok');
  }
}

/**
 * Purge a feature and all its content types.
 *
 * @param string $name
 *   The feature name.
 *
 * @throws \Exception
 */
function drush_content_purge_feature($name) {
  if ($name === NULL) {
    drush_log(dt('You must specify a feature name to purge.'), 'warning');
    return;
  }

  if (!$feature = features_load_feature($name)) {
    drush_log(dt('Could not load feature @name to purge.', ['@name' => $name]), 'warning');
    return;
  }

  drush_log(dt('Purge feature @name.', ['@name' => $name]), 'ok');

  // Disable and uninstall the feature.
  module_disable([$name]);
  drush_log(dt('  Disabled feature @name', ['@name' => $name]), 'ok');
  drupal_uninstall_modules([$name]);
  drush_log(dt('  Uninstalled feature @name', ['@name' => $name]), 'ok');

  // Purge each content type belonging to the feature.
  if (!empty($feature->info['features']['node'])) {
    foreach ($feature->info['features']['node'] as $bundle) {
      drush_content_purge_entity_bundle($bundle);
    }
  }

  // Purge each file type belonging to the feature.
  if (!empty($feature->info['features']['file_type'])) {
    foreach ($feature->info['features']['file_type'] as $file_type) {
      drush_content_purge_file_type($file_type);
    }
  }

  // Purge each field instance belonging to the feature.
  if (!empty($feature->info['features']['field_instance'])) {
    foreach ($feature->info['features']['field_instance'] as $field_instance) {
      list($entity_type, $bundle, $field_name) = explode('-', $field_instance);
      if ($instance = field_info_instance($entity_type, $field_name, $bundle)) {
        drush_op('field_delete_instance', $instance);
        drush_log(dt('  Field instance deleted: @field_name.', ['@field_name' => $field_name]), 'ok');
      }
    }
  }

  // Purge each view belonging to the feature.
  if (!empty($feature->info['features']['views_view'])) {
    foreach ($feature->info['features']['views_view'] as $view_name) {
      if ($view = views_get_view($view_name)) {
        views_delete_view($view);
        drush_log(dt('  Delete view @name', ['@name' => $view_name]), 'ok');
      }
    }
  }
}

/**
 * Purge fields and groups from a content type and re-export the feature.
 *
 * @param string $name
 *   The feature name.
 * @param string $bundle
 *   The bundle name.
 * @param string $fields
 *   A space-separated list of field names.
 * @param string $groups
 *   A space-separated list of group names.
 */
function drush_content_purge_feature_fields($name, $bundle, $fields, $groups) {
  if ($name === NULL) {
    drush_log(dt('You must specify a feature name.'), 'warning');
    return;
  }

  if ($bundle === NULL) {
    drush_log(dt('You must specify a content type bundle name.'), 'warning');
    return;
  }

  if ($fields === NULL && $groups === NULL) {
    drush_log(dt('Nothing to do. Specify fields and/or groups.'), 'warning');
    return;
  }

  if (!$feature = features_load_feature($name)) {
    drush_log(dt('Could not load feature @name.', ['@name' => $name]), 'warning');
    return;
  }

  $args = [
    '@bundle' => $bundle,
    '@name' => $name,
    '@fields' => $fields,
    '@groups' => $groups,
  ];
  // Get the entity type.
  $entity_type = drush_field_get_entity_from_bundle($bundle);

  // Purge the fields from the content type.
  if ($fields) {
    drush_log(dt('Purging fields from bundle @bundle in feature @name:', $args), 'ok');
    $field_names = explode(' ', $fields);
    foreach($field_names as $field_name) {
      if ($instance = field_info_instance($entity_type, $field_name, $bundle)) {
        drush_op('field_delete_instance', $instance);
        drush_log(dt('Field instance deleted: @field_name.', ['@field_name' => $field_name]), 'ok');
      }
      else {
        drush_log(dt('Field instance not found: @field_name.', ['@field_name' => $field_name]), 'warning');
      }
    }
  }

  // Purge the groups from the content type.
  // @TODO: May want to check if each group is empty first???
  if ($groups) {
    drush_log(dt('Purging groups from bundle @bundle in feature @name:', $args), 'ok');
    $group_ids = explode(' ', $groups);
    foreach ($group_ids as $group_id) {
      list($group_name, $entity_type, $bundle, $mode) = explode('|', $group_id);
      if ($group = field_group_load_field_group($group_name, $entity_type, $bundle, $mode)) {
        field_group_group_export_delete($group, FALSE);
        drush_log(dt('Group deleted: @group_name.', ['@group_name' => $group_name]), 'ok');
      }
      else {
        drush_log(dt('Group not found: @group_name.', ['@group_name' => $group_name]), 'warning');
      }
    }
  }

  // Purge deleted fields.
//  drush_log(dt('Purging deleted fields.'), 'ok');
//  field_purge_batch(1000);

  // Recreate the feature in code.
  if (!empty($feature->status)) {
    drush_log(dt('Re-creating feature: @name.', ['@name' => $name]), 'ok');
    drush_features_update($name);
  }
  else {
    drush_log(dt('Feature not enabled: @name.', ['@name' => $name]), 'warning');
  }
}

/**
 * Delete s3 files from file_managed.
 *
 * DELETE FROM file_managed WHERE uri LIKE 's3://%'
 */
function drush_content_purge_missing_files() {
  $fids = db_select('file_managed', 'f')
    ->fields('f', array('fid'))
    ->condition('f.uri', db_like('s3://') . '%', 'LIKE')
    ->execute()
    ->fetchCol();

  foreach ($fids as $fid) {
    drush_log(dt('Delete file id: @fid.', ['@fid' => $fid]), 'ok');
    $file = file_load($fid);
    // TODO: Replace this with file_delete_multiple() if it makes sense. Will delete file if found.
    _content_purge_missing_file($file);
  }
}
